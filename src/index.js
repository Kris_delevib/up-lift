import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import store from "./store/configStore";
import "./assets/style/font.scss"
import './assets/style/index.scss';
import "react-toastify/dist/ReactToastify.css";
import * as serviceWorker from './serviceWorker';
import {App} from './App';

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root'));

serviceWorker.register();
