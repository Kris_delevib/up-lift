import React from 'react';
import {Link} from 'react-router-dom';
import {PersonalInfo} from "../PersonalInfo/PersonalInfo";
import {GeneralInformation} from "../GeneralInformation/GeneraInformation";
import {Keywords} from "../Keywords/Keywords";
import {Assessments} from "../Assessments/Assessments";
import GalleryPhoto from "../GalleryPhoto/GalleryPhoto";
import {Avatar} from "../Avatar/Avatar";
import {Education} from "../Education/Education";
import {ButtonFavorites} from "../ButtonOptions/ButtonOptions";
import {AltIcon} from "../Icons/Alt";
import PPT from '../../assets/img/icon/ppt.png';
import DOC from '../../assets/img/icon/doc.png';

export const ResumeCard = ({resume, files, ...props}) => {
  const list = [
    { name: 'Город:', intent: resume.user.student.city, show: true},
    { name: 'Телефон:', intent: resume.user.phone, show: false},
    { name: 'Email:', intent: resume.user.email, show: false},
    { name: 'Готовность к командировкам:', intent: resume.trip, show: true},
    { name: 'Занятость:', intent: resume.type_employment, show: true},
  ];

   const disabled = props.feedback === 1;
   const favorite = props.favorites === 1;

  return (
      <>
        {props.inviteInfo && <MessageBlockInvite inviteInfo={props.inviteInfo} />}
        {props.infoFeedback && <MessageBlockFeedback infoFeedback={props.infoFeedback} />}
        <div className="inner-card inner-card--primary">
          <aside>
            <div className="card-aside mb">
              <div className="card-info card-bg">
                <div className="card-info__head card-info__head--center">
                  <Avatar photo={resume.user.photo} alt={`${resume.user.username} ${resume.user.surname}`} class="card-info__img card-info__img--circle" />

                  <h1 className="title-bold text-black">{resume.user.username} {resume.user.surname}</h1>
                  <div className="card-info__params">
                    <PersonalInfo studentData={resume.user.student} />
                  </div>
                </div>

                <div className="card-info__body">
                  <h3 className="title-bold text-upper">Общая информация</h3>
                  <ul>
                    <GeneralInformation list={list} checkAuth={props.checkAuth} condition={()=> props.checkAuth.id === resume.uid || props.checkAuth.type === 'hirer' || props.checkAuth.type === 'university'} />
                  </ul>
                </div>

                {props.checkAuth === undefined &&
                  <div className="card-info__body">
                    <p className="mb-none text-red">Персональные данные доступны только зарегистрированным пользователям</p>
                  </div>
                }

                <div className="card-info__foot">
                  <h3 className="title-bold text-upper">Ключевые навыки</h3>
                  <Keywords keywords={resume.keywords.split(", ")} classBadge="badge--primary" />
                </div>

              </div>
            </div>
          </aside>

          <div className="inner-card__content flex-element flex-element--justify-sb card-content">

            <div className="card-content__left card-content__left--no-indentation">
              <div className="card-about card-bg mb">

                <div className="card-about__head flex-element flex-element--options">
                  <div className="card-about__group flex-element flex-element--align-center">
                    <AltIcon />
                    <h3 className="title-bold mb-none">Желаемая должность</h3>
                  </div>
                  <div className="date">
                    {new Date(resume.created_at * 1000).toLocaleString('ru', {
                      year: 'numeric',
                      month: 'long',
                      day: 'numeric'
                    })}
                  </div>
                </div>

                <div className="card-about__body">
                  <h4 className="title-post title-inner-bold">{resume.post}</h4>
                  <div className="salary title-inner-bold">{resume.salary}</div>

                  {props.typeUser === 'hirer' &&
                    <div className="button-group button-group--pt">
                      <button className="button button-gradient-primary" disabled={disabled} onClick={props.handleOpen}>Пригласить</button>

                       {favorite ?
                          <ButtonFavorites onClick={props.handleRemoveFavorites} class="active" />
                          :
                          <ButtonFavorites onClick={props.handleAddFavorites} />
                       }

                    </div>
                  }
                </div>
              </div>

              {files.documents.length > 0 &&
                <div className="card-about card-bg mb">
                  <div className="card-about__head flex-element">
                    <svg width="21" height="21" viewBox="0 0 21 21" fill="none" xmlns="http://www.w3.org/2000/svg" aria-hidden="true">
                      <path d="M10 18H3C2.73478 18 2.48043 17.8946 2.29289 17.7071C2.10536 17.5196 2 17.2652 2 17V3C2 2.73478 2.10536 2.48043 2.29289 2.29289C2.48043 2.10536 2.73478 2 3 2H8V5C8 5.79565 8.31607 6.55871 8.87868 7.12132C9.44129 7.68393 10.2044 8 11 8H14V9C14 9.26522 14.1054 9.51957 14.2929 9.70711C14.4804 9.89464 14.7348 10 15 10C15.2652 10 15.5196 9.89464 15.7071 9.70711C15.8946 9.51957 16 9.26522 16 9V7C16 7 16 7 16 6.94C15.9896 6.84813 15.9695 6.75763 15.94 6.67V6.58C15.8919 6.47718 15.8278 6.38267 15.75 6.3L9.75 0.3C9.66734 0.222216 9.57282 0.158081 9.47 0.11C9.44015 0.10576 9.40985 0.10576 9.38 0.11C9.27841 0.0517412 9.16622 0.0143442 9.05 0H3C2.20435 0 1.44129 0.316071 0.87868 0.87868C0.316071 1.44129 0 2.20435 0 3V17C0 17.7956 0.316071 18.5587 0.87868 19.1213C1.44129 19.6839 2.20435 20 3 20H10C10.2652 20 10.5196 19.8946 10.7071 19.7071C10.8946 19.5196 11 19.2652 11 19C11 18.7348 10.8946 18.4804 10.7071 18.2929C10.5196 18.1054 10.2652 18 10 18ZM10 3.41L12.59 6H11C10.7348 6 10.4804 5.89464 10.2929 5.70711C10.1054 5.51957 10 5.26522 10 5V3.41ZM5 6C4.73478 6 4.48043 6.10536 4.29289 6.29289C4.10536 6.48043 4 6.73478 4 7C4 7.26522 4.10536 7.51957 4.29289 7.70711C4.48043 7.89464 4.73478 8 5 8H6C6.26522 8 6.51957 7.89464 6.70711 7.70711C6.89464 7.51957 7 7.26522 7 7C7 6.73478 6.89464 6.48043 6.70711 6.29289C6.51957 6.10536 6.26522 6 6 6H5ZM19.71 18.29L18.54 17.13C18.914 16.4773 19.0636 15.7199 18.9661 14.974C18.8686 14.2281 18.5292 13.5347 18 13C17.513 12.4957 16.8854 12.1497 16.199 12.0071C15.5126 11.8646 14.7992 11.9321 14.1517 12.2008C13.5041 12.4695 12.9526 12.927 12.5688 13.5137C12.185 14.1004 11.9868 14.7891 12 15.49C11.9966 16.0932 12.1509 16.6868 12.4476 17.2119C12.7444 17.7371 13.1733 18.1755 13.6917 18.4838C14.2102 18.7921 14.8003 18.9594 15.4034 18.9693C16.0065 18.9791 16.6017 18.8311 17.13 18.54L18.29 19.71C18.383 19.8037 18.4936 19.8781 18.6154 19.9289C18.7373 19.9797 18.868 20.0058 19 20.0058C19.132 20.0058 19.2627 19.9797 19.3846 19.9289C19.5064 19.8781 19.617 19.8037 19.71 19.71C19.8037 19.617 19.8781 19.5064 19.9289 19.3846C19.9797 19.2627 20.0058 19.132 20.0058 19C20.0058 18.868 19.9797 18.7373 19.9289 18.6154C19.8781 18.4936 19.8037 18.383 19.71 18.29ZM16.54 16.54C16.2544 16.8086 15.8771 16.9581 15.485 16.9581C15.0929 16.9581 14.7156 16.8086 14.43 16.54C14.1547 16.2598 14.0003 15.8828 14 15.49C13.9979 15.2928 14.0359 15.0971 14.1115 14.915C14.1871 14.7328 14.2989 14.5678 14.44 14.43C14.7066 14.1648 15.0641 14.0111 15.44 14C15.6422 13.9876 15.8447 14.0171 16.035 14.0866C16.2252 14.1562 16.399 14.2643 16.5455 14.4042C16.692 14.5441 16.808 14.7128 16.8862 14.8996C16.9644 15.0865 17.0031 15.2875 17 15.49C16.9917 15.8871 16.8263 16.2647 16.54 16.54ZM11 10H5C4.73478 10 4.48043 10.1054 4.29289 10.2929C4.10536 10.4804 4 10.7348 4 11C4 11.2652 4.10536 11.5196 4.29289 11.7071C4.48043 11.8946 4.73478 12 5 12H11C11.2652 12 11.5196 11.8946 11.7071 11.7071C11.8946 11.5196 12 11.2652 12 11C12 10.7348 11.8946 10.4804 11.7071 10.2929C11.5196 10.1054 11.2652 10 11 10ZM9 16C9.26522 16 9.51957 15.8946 9.70711 15.7071C9.89464 15.5196 10 15.2652 10 15C10 14.7348 9.89464 14.4804 9.70711 14.2929C9.51957 14.1054 9.26522 14 9 14H5C4.73478 14 4.48043 14.1054 4.29289 14.2929C4.10536 14.4804 4 14.7348 4 15C4 15.2652 4.10536 15.5196 4.29289 15.7071C4.48043 15.8946 4.73478 16 5 16H9Z" fill="#99AAC3"/>
                    </svg>
                    <h3 className="title-bold mb-none">Документы</h3>
                  </div>

                  <div className="card-about__body">
                    <div className="documents-block flex-element">
                      {files.documents.map(file => {
                        if (file.path.endsWith('.docx') || file.path.endsWith('.doc')) {
                          return <Link target="_blank" to={{pathname: `${file.path}`}} className="document-preview" key={file.id}>
                             <img className="icon" src={DOC} alt="" aria-hidden="true"/>
                            <span>{file.name}</span>
                          </Link>
                        } else {
                          return <Link target="_blank" to={{pathname: `${file.path}`}} className="document-preview" key={file.id}>
                             <img className="icon" src={PPT} alt="" aria-hidden="true" />
                            <span>{file.name}</span>
                          </Link>
                        }
                      })}
                    </div>
                  </div>
                </div>
              }

              <div className="card-about card-bg mb">
                <div className="card-about__head flex-element">
                   <AltIcon />
                  <h3 className="title-bold mb-none">О себе</h3>
                </div>
                <div className="card-about__body">
                  {resume.about}
                </div>
              </div>

               <div className="card-about card-bg mb">
                  <div className="card-about__head flex-element">
                     <AltIcon />
                     <h3 className="title-bold mb-none">Профессиональная область:</h3>
                  </div>
                  <div className="card-about__body">
                     <Keywords keywords={resume.category} classBadge="badge--light" />
                  </div>
               </div>

               {resume.work.length !== 0 &&
                  <div className="card-about card-bg mb">
                     <div className="card-about__head flex-element">
                        <svg width="25" height="24" viewBox="0 0 25 24" fill="none" xmlns="http://www.w3.org/2000/svg" aria-hidden="true">
                           <path d="M19.8718 6.5H16.8794V5.5C16.8794 4.70435 16.5641 3.94129 16.0029 3.37868C15.4417 2.81607 14.6806 2.5 13.8869 2.5H11.8919C11.0983 2.5 10.3371 2.81607 9.77595 3.37868C9.21475 3.94129 8.89947 4.70435 8.89947 5.5V6.5H5.90701C5.11336 6.5 4.35222 6.81607 3.79102 7.37868C3.22983 7.94129 2.91455 8.70435 2.91455 9.5V18.5C2.91455 19.2956 3.22983 20.0587 3.79102 20.6213C4.35222 21.1839 5.11336 21.5 5.90701 21.5H19.8718C20.6655 21.5 21.4266 21.1839 21.9878 20.6213C22.549 20.0587 22.8643 19.2956 22.8643 18.5V9.5C22.8643 8.70435 22.549 7.94129 21.9878 7.37868C21.4266 6.81607 20.6655 6.5 19.8718 6.5ZM10.8944 5.5C10.8944 5.23478 10.9995 4.98043 11.1866 4.79289C11.3737 4.60536 11.6274 4.5 11.8919 4.5H13.8869C14.1515 4.5 14.4052 4.60536 14.5922 4.79289C14.7793 4.98043 14.8844 5.23478 14.8844 5.5V6.5H10.8944V5.5ZM20.8693 18.5C20.8693 18.7652 20.7642 19.0196 20.5772 19.2071C20.3901 19.3946 20.1364 19.5 19.8718 19.5H5.90701C5.64246 19.5 5.38875 19.3946 5.20168 19.2071C5.01462 19.0196 4.90953 18.7652 4.90953 18.5V13C5.88252 13.3869 6.88274 13.7011 7.90199 13.94V14.53C7.90199 14.7952 8.00708 15.0496 8.19414 15.2371C8.38121 15.4246 8.63492 15.53 8.89947 15.53C9.16402 15.53 9.41774 15.4246 9.6048 15.2371C9.79187 15.0496 9.89696 14.7952 9.89696 14.53V14.32C10.8888 14.4554 11.8884 14.5255 12.8894 14.53C13.8904 14.5255 14.89 14.4554 15.8819 14.32V14.53C15.8819 14.7952 15.987 15.0496 16.174 15.2371C16.3611 15.4246 16.6148 15.53 16.8794 15.53C17.1439 15.53 17.3976 15.4246 17.5847 15.2371C17.7718 15.0496 17.8769 14.7952 17.8769 14.53V13.94C18.8961 13.7011 19.8963 13.3869 20.8693 13V18.5ZM20.8693 10.81C19.8992 11.2205 18.8987 11.5548 17.8769 11.81V11.5C17.8769 11.2348 17.7718 10.9804 17.5847 10.7929C17.3976 10.6054 17.1439 10.5 16.8794 10.5C16.6148 10.5 16.3611 10.6054 16.174 10.7929C15.987 10.9804 15.8819 11.2348 15.8819 11.5V12.24C13.8981 12.54 11.8807 12.54 9.89696 12.24V11.5C9.89696 11.2348 9.79187 10.9804 9.6048 10.7929C9.41774 10.6054 9.16402 10.5 8.89947 10.5C8.63492 10.5 8.38121 10.6054 8.19414 10.7929C8.00708 10.9804 7.90199 11.2348 7.90199 11.5V11.83C6.88018 11.5748 5.87969 11.2405 4.90953 10.83V9.5C4.90953 9.23478 5.01462 8.98043 5.20168 8.79289C5.38875 8.60536 5.64246 8.5 5.90701 8.5H19.8718C20.1364 8.5 20.3901 8.60536 20.5772 8.79289C20.7642 8.98043 20.8693 9.23478 20.8693 9.5V10.81Z" fill="#99AAC3"/>
                        </svg>
                        <h3 className="title-bold mb-none">Опыт работы:</h3>
                     </div>
                     <div className="card-about__body">
                        <ul className="list-style list-experience">
                           {resume.work.map(work => {
                              return (
                                 <div className="work flex-element" key={work.id}>
                                    <div className="work__dots">
                                       <svg width="9" height="8" viewBox="0 0 9 8" fill="none" xmlns="http://www.w3.org/2000/svg" aria-hidden="true">
                                          <ellipse cx="4.90963" cy="4" rx="3.98995" ry="4" fill="#27AE60"/>
                                       </svg>
                                    </div>
                                    <div className="work__group">
                                       <div className="work__experience-count">
                                          {`${work.start_month} ${work.start} - `}
                                          {work.until_now === 1 ? 'по настоящее время' : `${work.end_month} ${work.end}`}
                                       </div>
                                       <div className="work__name-organization">{work.organization_name}</div>
                                       <div className="title-bold work__name-post">{work.post}</div>
                                       <p dangerouslySetInnerHTML={{__html: work.responsibility.replace(/\n/g, '<br>').trim()}}/>
                                    </div>
                                 </div>
                              )
                           })}
                        </ul>
                     </div>
                  </div>
               }
            </div>

            <div className="card-content__right">
              <div className="card-bg mb inner-card-body inner-card-body--params">
                <h3 className="title-bold">Образование</h3>
                 <Education education={resume.user.student} />
              </div>

              <div className="card-bg mb inner-card-body">
                <h3 className="title-bold">Оценки</h3>
                <Assessments assessments={resume.user.student.details.assessments} />
              </div>

              {files.portfolio.length > 0 &&
                <div className="card-bg card-photo">
                  <h3 className="title-bold">Фотогалерея</h3>
                  <GalleryPhoto gallery={files.portfolio} />
                </div>
              }
            </div>
          </div>
        </div>
      </>
  )
};

const MessageBlockInvite = ({inviteInfo}) => {
  return (
      <div className="blog-head">
        <div className="bg-card message-block message-block--mb">
          <div className="flex-element flex-element--options">
            <h4 className="min title-semi-bold dark mb-none">Ваше сообщение</h4>
            <div className="date min">{new Date(inviteInfo.created_at * 1000).toLocaleString('ru', {
              year: 'numeric',
              month: 'numeric',
              day: 'numeric'
            })}</div>
          </div>
          <div className="message-block__content">{inviteInfo.message}</div>
        </div>
      </div>
  )
};

const MessageBlockFeedback = ({infoFeedback}) => {
  return (
      <div className="blog-head">
        <div className="bg-card message-block message-block--mb">
          <div className="flex-element flex-element--options">
            <h4 className="min title-semi-bold dark mb-none">Сообщение от кандидата</h4>
            <div className="date min">{new Date(infoFeedback.created_at * 1000).toLocaleString('ru', {
              year: 'numeric',
              month: 'numeric',
              day: 'numeric'
            })}</div>
          </div>
          <div className="message-block__content">{infoFeedback.message}!</div>
        </div>
      </div>
  )
};
