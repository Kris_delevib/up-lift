import React from "react";
import { Field, reduxForm } from "redux-form";
import validate from "../FormElement/validate";
import { FieldInput } from "../FormElement/Field/FieldInput";
import { UploadFile } from "../FormElement/Field/UploadDile";

function UploadFileGroups({ handleSubmit }) {
   return (
      <form className="form form--upload" onSubmit={(e) => handleSubmit(e)}>
         <div className="upload">
            <label htmlFor="file" aria-label="Загрузить из файла">
               <Field
                  id="file"
                  name="file"
                  type="file"
                  accept=".xlsx"
                  component={UploadFile}
               />
            </label>
         </div>

         <div className="flex-element form__group">
            <div className="item">
               <label htmlFor="name_start">
                  Введите номер ячейки с наименованием группы
               </label>
               <Field
                  name="name_start"
                  id="name_start"
                  component={FieldInput}
                  type="text"
                  placeholder="например, A3"
               />
            </div>

            <div className="item">
               <label htmlFor="training_start">
                  Введите номер ячейки с типом обучения
               </label>
               <Field
                  name="training_start"
                  id="training_start"
                  component={FieldInput}
                  type="text"
                  placeholder="например, B3"
               />
            </div>

            <div className="item">
               <label htmlFor="study_start">
                  Введите номер ячейки с формой обучения
               </label>
               <Field
                  name="study_start"
                  id="study_start"
                  component={FieldInput}
                  type="text"
                  placeholder="например, C3"
               />
            </div>

            <div className="item">
               <label htmlFor="start_start">
                  Введите номер ячейки с началом года обучения
               </label>
               <Field
                  name="start_start"
                  id="start_start"
                  component={FieldInput}
                  type="text"
                  placeholder="например, D3"
               />
            </div>

            <div className="item">
               <label htmlFor="end_start">
                  Введите номер ячейки с окончанием учебы
               </label>
               <Field
                  name="end_start"
                  id="end_start"
                  component={FieldInput}
                  type="text"
                  placeholder="например, E3"
               />
            </div>
         </div>

         <button className="button button-gradient-primary button-indent button-submit">
            Сохранить
         </button>
      </form>
   );
}

export default reduxForm({
   form: "file",
   validate,
})(UploadFileGroups);
