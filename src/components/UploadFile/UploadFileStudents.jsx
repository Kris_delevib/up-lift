import React from "react";
import { Field, reduxForm } from "redux-form";
import validate from "../FormElement/validate";
import { FieldInput } from "../FormElement/Field/FieldInput";
import { UploadFile } from "../FormElement/Field/UploadDile";

function UploadFileStudents({ handleSubmit }) {
   return (
      <form className="form form--upload" onSubmit={(e) => handleSubmit(e)}>
         <div className="upload">
            <label htmlFor="file" aria-label="Загрузить из файла">
               <svg
                  width="21"
                  height="16"
                  viewBox="0 0 21 16"
                  fill="none"
                  xmlns="http://www.w3.org/2000/svg"
                  aria-hidden="true"
               >
                  <path
                     d="M17.42 4.22038C16.809 2.81639 15.7545 1.65146 14.4181 0.90411C13.0817 0.15676 11.5371 -0.131785 10.021 0.0826925C8.50489 0.29717 7.10094 1.00284 6.02427 2.09156C4.9476 3.18028 4.2576 4.592 4.06001 6.11038C3.1066 6.33871 2.27023 6.90922 1.70975 7.71357C1.14927 8.51793 0.903696 9.50011 1.01968 10.4736C1.13566 11.4471 1.60511 12.3441 2.33888 12.9942C3.07265 13.6444 4.01964 14.0024 5.00001 14.0004C5.26523 14.0004 5.51958 13.895 5.70712 13.7075C5.89466 13.52 6.00001 13.2656 6.00001 13.0004C6.00001 12.7352 5.89466 12.4808 5.70712 12.2933C5.51958 12.1057 5.26523 12.0004 5.00001 12.0004C4.46958 12.0004 3.96087 11.7897 3.5858 11.4146C3.21073 11.0395 3.00001 10.5308 3.00001 10.0004C3.00001 9.46995 3.21073 8.96124 3.5858 8.58617C3.96087 8.2111 4.46958 8.00038 5.00001 8.00038C5.26523 8.00038 5.51958 7.89503 5.70712 7.70749C5.89466 7.51995 6.00001 7.2656 6.00001 7.00038C6.00257 5.81766 6.4243 4.67414 7.19028 3.77297C7.95627 2.8718 9.0169 2.27133 10.1838 2.07826C11.3506 1.88518 12.5481 2.11199 13.5636 2.71839C14.579 3.32479 15.3466 4.27152 15.73 5.39038C15.7872 5.56222 15.8899 5.71532 16.0273 5.83332C16.1647 5.95132 16.3315 6.0298 16.51 6.06038C17.1761 6.18625 17.7799 6.53399 18.223 7.04692C18.6662 7.55985 18.9226 8.20774 18.9504 8.88502C18.9782 9.56231 18.7759 10.2291 18.3763 10.7766C17.9767 11.3242 17.4035 11.7203 16.75 11.9004C16.4928 11.9667 16.2724 12.1325 16.1373 12.3613C16.0023 12.5901 15.9637 12.8631 16.03 13.1204C16.0963 13.3776 16.2621 13.598 16.4909 13.7331C16.7197 13.8681 16.9928 13.9067 17.25 13.8404C18.3024 13.5623 19.2353 12.9484 19.907 12.0918C20.5787 11.2353 20.9526 10.183 20.9718 9.09465C20.9911 8.00631 20.6546 6.94143 20.0136 6.0617C19.3725 5.18198 18.4619 4.53548 17.42 4.22038ZM11.71 6.29038C11.6149 6.19934 11.5028 6.12798 11.38 6.08038C11.1366 5.98037 10.8635 5.98037 10.62 6.08038C10.4973 6.12798 10.3851 6.19934 10.29 6.29038L7.29001 9.29038C7.10171 9.47869 6.99592 9.73408 6.99592 10.0004C6.99592 10.2667 7.10171 10.5221 7.29001 10.7104C7.47832 10.8987 7.73371 11.0045 8.00001 11.0045C8.26631 11.0045 8.52171 10.8987 8.71001 10.7104L10 9.41038V15.0004C10 15.2656 10.1054 15.52 10.2929 15.7075C10.4804 15.895 10.7348 16.0004 11 16.0004C11.2652 16.0004 11.5196 15.895 11.7071 15.7075C11.8947 15.52 12 15.2656 12 15.0004V9.41038L13.29 10.7104C13.383 10.8041 13.4936 10.8785 13.6154 10.9293C13.7373 10.98 13.868 11.0062 14 11.0062C14.132 11.0062 14.2627 10.98 14.3846 10.9293C14.5064 10.8785 14.617 10.8041 14.71 10.7104C14.8037 10.6174 14.8781 10.5068 14.9289 10.385C14.9797 10.2631 15.0058 10.1324 15.0058 10.0004C15.0058 9.86837 14.9797 9.73767 14.9289 9.61581C14.8781 9.49395 14.8037 9.38335 14.71 9.29038L11.71 6.29038Z"
                     fill="white"
                  />
               </svg>

               <Field
                  id="file"
                  name="file"
                  type="file"
                  accept=".xlsx"
                  component={UploadFile}
               />
            </label>
         </div>

         <div className="flex-element form__group">
            <div className="item">
               <label htmlFor="surname_start">
                  Введите номер ячейки с фамилией студента
               </label>
               <Field
                  name="surname_start"
                  id="surname_start"
                  component={FieldInput}
                  type="text"
                  placeholder="например, A3"
               />
            </div>

            <div className="item">
               <label htmlFor="name_start">
                  Введите номер ячейки с именем студента
               </label>
               <Field
                  name="name_start"
                  id="name_start"
                  component={FieldInput}
                  type="text"
                  placeholder="например, B3"
               />
            </div>

            <div className="item">
               <label htmlFor="patronymic_start">
                  Введите номер ячейки с отчеством студента
               </label>
               <Field
                  name="patronymic_start"
                  id="patronymic_start"
                  component={FieldInput}
                  type="text"
                  placeholder="например, C3"
               />
            </div>

            <div className="item">
               <label htmlFor="ticket_start">
                  Введите номер ячейки с номером зачетной книжки
               </label>
               <Field
                  name="ticket_start"
                  id="ticket_start"
                  component={FieldInput}
                  type="text"
                  placeholder="например, D3"
               />
            </div>
            <div className="item">
               <label htmlFor="groups_start">
                  Введите номер ячейки с наименованием группы
               </label>
               <Field
                  name="groups_start"
                  id="ticket_start"
                  component={FieldInput}
                  type="text"
                  placeholder="например, E3"
               />
            </div>
         </div>

         <button className="button button-gradient-primary button-indent button-submit">
            Сохранить
         </button>
      </form>
   );
}

export default reduxForm({
   form: "file",
   validate,
})(UploadFileStudents);
