const top = 30;
const left = 50;

export const getModalStyle = {
   top: `${top}%`,
   left: `${left}%`,
   transform: `translate(-50%, -${left}%)`,
   position: 'absolute',
};
