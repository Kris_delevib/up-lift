import axios from "axios";

export const API = 'https://api.upliftplatform.ru/v1';

export default axios.create({
   baseURL: "https://api.upliftplatform.ru/v1"
});
