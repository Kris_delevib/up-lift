import React, { useEffect } from "react";
import { Switch, Route, Router, useLocation } from "react-router-dom";
import { history } from "./helpers/history";
import { Header } from "./components/Header/Header";
import { Home } from "./pages/Home";
import { About } from "./pages/About";
import { Help } from "./pages/Help";
import { Footer } from "./components/Footer/Footer";
import { LoginPage } from "./pages/LoginPage";
import { Registration } from "./pages/Registration";
import { RestorePassword } from "./components/RestorePassword/RestorePassword";
import { PersonalEmployerPage } from "./pages/PersonalEmployerPage";
import { UniversitiesPage } from "./pages/UniversitiesPage";
import { CompaniesPage } from "./pages/CompaniesPage";
import { PersonalAreaStudents } from "./pages/PersonalAreaStudents";
import { ResumePage } from "./pages/ResumePage";
import { VacanciesPage } from "./pages/VacanciesPage";
import { PersonalAreaUniversity } from "./pages/PersonalAreaUniversity";
import PrivateRouteUniversity from "./helpers/PrivateRouteUniversity";
import PrivateRouteEmployer from "./helpers/PrivateRouteEmployer";
import PrivateRouteStudent from "./helpers/PrivateRouteStudent";
import { useDispatch } from "react-redux";
import { userActions } from "./actions/AuthAction";
import { StudentsPage } from "./pages/StudentsPage";
import { NotFount } from "./components/NotFound/NotFound";
import { ToastContainer } from "react-toastify";
import { ToolBarBlocked } from "./components/Toolbar/ToolBarBlocked";

function ScrollToTop() {
   const { pathname } = useLocation();

   useEffect(() => {
      window.scrollTo(0, 0);
   }, [pathname]);

   return null;
}

export const App = () => {
   const dispatch = useDispatch();

   window.addEventListener("storage", (e) => {
      if (e.key === "token" && e.oldValue && !e.newValue) {
         dispatch(userActions.logout());
      }

      if (e.key === "token" && !e.oldValue && e.newValue) {
         history.push("/");
         window.location.reload();
      }
   });

   return (
      <div className="wrapper">
         <ToastContainer autoClose={3000} />
         <Router history={history}>
            <ScrollToTop />
            <Header />
            <main>
               <Switch>
                  <Route exact path="/" component={Home} />
                  <Route path="/resumes" component={ResumePage} />
                  <Route path="/vacancies" component={VacanciesPage} />
                  <Route path="/companies" component={CompaniesPage} />
                  <Route path="/universities" component={UniversitiesPage} />
                  <Route path="/students" component={StudentsPage} />
                  <Route path="/help" component={Help} />
                  <Route path="/about" component={About} />
                  <Route path="/login" component={LoginPage} />
                  <Route path="/restore" component={RestorePassword} />
                  <Route path="/registration" component={Registration} />
                  <PrivateRouteStudent
                     path="/student"
                     component={PersonalAreaStudents}
                  />
                  <PrivateRouteEmployer
                     path="/employer"
                     component={PersonalEmployerPage}
                  />
                  <PrivateRouteUniversity
                     path="/university"
                     component={PersonalAreaUniversity}
                  />
                  <Route component={NotFount} />
               </Switch>
            </main>
            <Footer />
            /* сообщение о блокирокки */
            <ToolBarBlocked />
         </Router>
      </div>
   );
};
